﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Threading;

namespace ConnMgrClient
{
    class TcpCommMgr
    {
        TcpClient tc;
        public NetworkStream stream;
        int serverPort;
        bool runThreadRx;
        Thread threadRx;
        AsamXil4CANoe canoe;
        Matlab matlab;
        public TcpCommMgr(string canoeCfgPath, string imfPath, int port)
        {
            serverPort = port;
            runThreadRx = false;
            canoe = new AsamXil4CANoe(this, canoeCfgPath, imfPath);
            matlab = new Matlab(this);
        }

        public void Release()
        {
            if (runThreadRx == false)
                return;

            runThreadRx = false;
            try
            {
                stream.Close();
                tc.Close();
            }
            catch
            {

            }
            canoe.Release();
        }

        public bool Start()
        {
            try
            {
                tc = new TcpClient("127.0.0.1", serverPort);
                stream = tc.GetStream(); 
            }
            catch (Exception e)
            {
                return false;
            }

            LogViewManager.AddLogMessage("Client", tc.Client.RemoteEndPoint.ToString());
            runThreadRx = true;
            threadRx = new Thread(new ThreadStart(runReceive));
            threadRx.IsBackground = true;
            threadRx.Start();
            Send("2,2,2,1;");
            return true;
        }

        public void Send(string msg)
        {
            byte[] sendBuf = Encoding.ASCII.GetBytes(msg);
            stream.Write(sendBuf, 0, sendBuf.Length);
            LogViewManager.AddLogMessage("Tx", msg);
        }


        private void runReceive()
        {
            byte[] buf = new byte[1024];
            while (runThreadRx)
            {
                int nbytes = 0;
                try
                {
                    nbytes = stream.Read(buf, 0, buf.Length);
                }
                catch
                {
                    return;
                }
                string packet = Encoding.ASCII.GetString(buf, 0, nbytes);   //2,3,2,x;
                string[] arr = packet.Split(';');
                foreach (string s in arr)
                {
                    if (s.Equals(""))
                        continue;
                    processTcpPacket(s);
                }

            }
        }

        private void processTcpPacket(string packet)
        {
            LogViewManager.AddLogMessage("Rx", packet);
            //2,3,2,x
            string[] arr = packet.Split(',');
            if (arr.Length != 4)
                return;
            int msgType;
            int dataType;
            int ctrlType;

            try
            {
                msgType = Int32.Parse(arr[1]);
                dataType = Int32.Parse(arr[2]);
                ctrlType = Int32.Parse(arr[3]);
            }
            catch
            {
                return;
            }

            if (msgType != 3 || dataType != 2)
                return;
            if (ctrlType == 1)
            {
                canoe.Open();
            }
            else if (ctrlType == 2)
            {
                canoe.Start();
            }
            else if (ctrlType == 3)
            {
                canoe.Stop();
            }
            else if (ctrlType == 0xff)
            {
                MainWindow mainWnd = (MainWindow)System.Windows.Application.Current.MainWindow;
                mainWnd.CloseWindow();
            }
            else if (ctrlType == 5)
            {
                matlab.Open();
            }
            else
            {

            }
        }
    }
}
