﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnMgrClient
{
    class LogViewManager
    {
        public static event Action<string> LogMessageAdded;
        private static LogViewManager _instance;
        public static LogViewManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LogViewManager();
                }
                return _instance;
            }
        }

        public static void AddLogMessage(string type, string message)
        {
            LogMessageAdded?.Invoke(String.Format($"[{type}] {message}"));
        }
    }
}
