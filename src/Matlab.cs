﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnMgrClient
{
    class Matlab
    {
        string matlabPath;    //절대경로
        TcpCommMgr tcp;

        public Matlab(TcpCommMgr parent)
        {
            this.matlabPath = "C:/Program Files/MATLAB/R2024a/bin/matlab.exe";
            this.tcp = parent;
        }

        public void Open()
        {
            Process.Start(matlabPath);
            tcp.Send("2,1,1,5 Matlab Load();");
        }
    }
}
