﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ASAM.XIL.Interfaces.Testbench;
using ASAM.XIL.Interfaces.Testbench.MAPort;
using ASAM.XIL.Implementation.TestbenchFactory.Testbench;

namespace ConnMgrClient
{
    class AsamXil4CANoe
    {
        //     string sManifestFolder = "../../asamxil_imf";
        //     string sManifestFolder = "D:/1_workspace/230727_VirtualECU/dev/connectmgrclient/src/asamxil_imf";

        string sVendorName = "DRIMAES";
        string sProductName = "DRIMCAVE";
        string sProductVersion = "1.1.0.0";
        string canoeCfgPath;    //절대경로
        IMAPort sMAPort;
        TcpCommMgr tcp;
        public AsamXil4CANoe(TcpCommMgr parent, string canoeCfgPath, string imfPath)
        {
            TestbenchFactory factory = new TestbenchFactory(imfPath);
            ITestbench testbench = factory.CreateVendorSpecificTestbench(sVendorName, sProductName, sProductVersion);
            sMAPort = testbench.MAPortFactory.CreateMAPort("");
            this.canoeCfgPath = canoeCfgPath;
            this.tcp = parent;
        }

        public void Open()
        {
            sMAPort.LoadConfiguration(canoeCfgPath);
            tcp.Send("2,1,1,[ASAMXIL] IMAPort LoadConfiguration();");
        }

        public void Start()
        {
            sMAPort.StartSimulation();
            tcp.Send("2,1,1,[ASAMXIL] IMAPort StartSimulation();");
        }

        public void Stop()
        {
            sMAPort.StopSimulation();
            tcp.Send("2,1,1,[ASAMXIL] IMAPort StopSimulation();");
        }

        public void Release()
        {
            tcp.Send("2,1,1,[ASAMXIL] IMAPort Disconnect();");
            sMAPort.Disconnect();
        }
    }
}
