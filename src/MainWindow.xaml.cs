﻿using ASAM.XIL.Implementation.TestbenchFactory.Testbench;
using ASAM.XIL.Interfaces.Testbench.MAPort;
using ASAM.XIL.Interfaces.Testbench;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Diagnostics;

namespace ConnMgrClient
{
    public partial class MainWindow : Window 
    {
        List<string> listLogData = new List<string>();
        const string toolInfo = "ConnMgrClient";
        string canoeCfgPath;
        int serverPort;
        Matlab matlab;
        TcpCommMgr client = null;
        Boolean debugMode = false;

        public MainWindow()
        {
            InitializeComponent();
            CheckAndPreventDuplicateExecution();
            CreateSystemTray();
            InitLogView();
            ParseIni();
            if (debugMode)
                this.Visibility = Visibility.Visible;
            RunTcpClient();
        }


        void RunTcpClient()
        {
            string imfPath = System.Windows.Forms.Application.StartupPath;
            client = new TcpCommMgr(canoeCfgPath, imfPath, serverPort);
            if (client.Start() == false)
                CloseWindow();
        }
        void CreateSystemTray()
        {
            NotifyIcon ni = new NotifyIcon();
            System.Windows.Forms.ContextMenu menu = new System.Windows.Forms.ContextMenu();

            System.Windows.Forms.MenuItem item1 = new System.Windows.Forms.MenuItem();
            item1.Index = 0;
            item1.Text = "Close";

            item1.Click += delegate (object click, EventArgs eClick)
            {
                CloseWindow();
            };
            menu.MenuItems.Add(item1);


            ni.Icon = Properties.Resources.Icon1;
            ni.Visible = true;
            ni.DoubleClick += delegate (object senders, EventArgs args)
            {
                System.Windows.MessageBox.Show("ConnMgrClient", toolInfo);
            };
            ni.ContextMenu = menu;
            ni.Text = toolInfo;
        }
        void InitLogView() 
        {
            uiLbLog.ItemsSource = listLogData;
            uiLbLog.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            LogViewManager.LogMessageAdded += AddLog;
        }

        private void AddLog(string message)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
            {
                listLogData.Add(message);
                uiLbLog.Items.Refresh();
                uiLbLog.SelectedIndex = uiLbLog.Items.Count - 1;
                uiLbLog.ScrollIntoView(uiLbLog.SelectedItem);
            }));
        }

        private void Button_Click_Clear(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
            {
                listLogData.Clear();
                uiLbLog.Items.Refresh();
            }));

        }


        void ParseIni()
        {
            string iniPath = String.Format("{0}\\sys.ini", System.Windows.Forms.Application.StartupPath);
            const string iniSection = "system";
            const string iniKeyPort = "port";
            const string iniKeyCanoeCfg = "canoecfg";
            const string iniKeyDebug = "debug";

            string sPort = IniParser.GetValue(iniPath, iniSection, iniKeyPort, "11191");
            string sDebug = IniParser.GetValue(iniPath, iniSection, iniKeyDebug, "0");
            try
            {
                serverPort = Int32.Parse(sPort);
                debugMode = Int32.Parse(sDebug) == 1 ? true : false ;
            }
            catch
            {
                serverPort = 11191; 
            }

            canoeCfgPath = IniParser.GetValue(iniPath, iniSection, iniKeyCanoeCfg, "");
            
        }

        public void CloseWindow()
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                if (client != null)
                    client.Release();
                this.Close();
            }));
        }

        #region CheckAndPreventDuplicateExecution
        Mutex mutex = null;
        const string mutexName = "ConnMgrClientAvoidDuplicateExecution";
        bool createdNewMutex = false;
        private void CheckAndPreventDuplicateExecution()
        {
            mutex = new Mutex(true, mutexName, out createdNewMutex);
            if (createdNewMutex == false)
            {
                System.Windows.MessageBox.Show("Program already running. Cannot start another instance.", toolInfo);
                CloseWindow();
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            if (createdNewMutex)
                mutex.ReleaseMutex();
        }
        #endregion



        #region test
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string sVendorName = "DRIMAES";
            string sProductName = "DRIMCAVE";
            string sProductVersion = "1.1.0.0";
            string imfPath = System.Windows.Forms.Application.StartupPath;
            TestbenchFactory factory = new TestbenchFactory(imfPath);
            ITestbench testbench = factory.CreateVendorSpecificTestbench(sVendorName, sProductName, sProductVersion);

            IMAPort sMAPort = testbench.MAPortFactory.CreateMAPort("");
            sMAPort.LoadConfiguration(canoeCfgPath);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string matlabPath = "C:/Program Files/MATLAB/R2024a/bin/matlab.exe";
            Process.Start(matlabPath);
        }

        #endregion

    }
}
